
FROM debian:9 as build
RUN apt update && apt install -y wget gcc make build-essential libpcre++-dev libssl-dev libgeoip-dev libxslt1-dev libpcre3 libpcre3-dev zlib1g zlib1g-dev



RUN wget http://nginx.org/download/nginx-1.19.6.tar.gz && tar xvfz nginx-1.19.6.tar.gz

RUN cd nginx-1.19.6 && ./configure \
&& make && make install


FROM debian:9

RUN mkdir -p /usr/local/nginx/logs /usr/local/nginx/conf
COPY --from=build /usr/local/nginx/sbin/nginx /usr/local/nginx/sbin/
COPY --from=build /usr/local/nginx/conf/* /usr/local/nginx/conf/
COPY --from=build /usr/local/nginx/ /usr/local/nginx/
RUN touch usr/local/nginx/logs/error.log && chmod +x /usr/local/nginx/sbin/nginx
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]

#docker run --mount type=bind,source=$(pwd)/nginx.conf,target=/usr/local/nginx/conf/nginx.conf --name=nginx_for_less4vf --cpus=".5" -d -p 8008:80 less4:v1
